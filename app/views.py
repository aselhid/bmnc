# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, JsonResponse
from .models import Universitas
from .raw_sql_utils import create_new_narasumber, create_new_mahasiswa, create_new_dosen, create_new_staf, get_berita,  get_tag, get_berita_by_tag, get_user_profil, get_berita_by_user, get_polling_biasa, is_user_with_npm_or_nik_exists, submit_polls, create_new_berita, create_new_polling, create_new_polling_berita, create_new_polling_biasa, create_new_respon, is_university_id_exists

import re
# Create your views here.


def index(request):
    response = {
        'page_title': 'Home page | BMNC',
        'beritas': get_berita(),
        'tags': get_tag()
    }

    return render(request, 'homepage/index.html', response)

def register(request):
    if request.user.is_anonymous:
        response = {
            'page_title': 'Register | BMNC',
        }

        if request.method == 'POST':
            role = request.POST.get('role')
            username = request.POST.get('username')
            password = request.POST.get('password')
            id_number = request.POST.get('id_number')
            email = request.POST.get('email')
            name = request.POST.get('name')
            birthplace = request.POST.get('birthplace')
            birthdate = request.POST.get('birthdate')
            phone_number = request.POST.get('phone_number')
            university_id = request.POST.get('university_id')
            student_status = request.POST.get('student_status')
            jurusan = request.POST.get('jurusan')
            jabatan = request.POST.get('jabatan')

            # Create narasumber row then create role row
            id_narasumber = create_new_narasumber(
                name, email, birthplace, birthdate, phone_number, university_id)
            if role == 'mahasiswa':
                create_new_mahasiswa(id_narasumber, id_number, student_status)
            elif role == 'staf':
                create_new_staf(id_narasumber, id_number, jabatan)
            elif role == 'dosen':
                create_new_dosen(id_narasumber, id_number, jurusan)

            User.objects.create_user(username, email, password)
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect('/')

        return render(request, 'register_page/index.html', response)
    return HttpResponseRedirect('/')

@csrf_exempt
def validate_register_request(request):
    role = request.POST.get('role')
    username = request.POST.get('username')
    password = request.POST.get('password')
    id_number = request.POST.get('id_number')
    name = request.POST.get('name')
    birthplace = request.POST.get('birthplace')
    birthdate = request.POST.get('birthdate')
    email = request.POST.get('email')
    phone_number = request.POST.get('phone_number')
    student_status = request.POST.get('student_status')
    jabatan = request.POST.get('jabatan')
    jurusan = request.POST.get('jurusan')
    university_id = request.POST.get('university_id')

    if User.objects.filter(username=username).exists():
        print('username exists')
        return JsonResponse({'success': False, 'error': 'Username exists'})

    if is_user_with_npm_or_nik_exists(id_number):
        return JsonResponse({'success': False, 'error': 'NIK/NPM exists'})

    if not re.match("[a-zA-Z0-9]{8,}", password):
        return JsonResponse({'success': False, 'error': 'Password must be 8 characters long and only contains alphanumeric character.'})

    if role != 'mahasiswa' and role != 'staf' and role != 'dosen':
        print('role')
        return JsonResponse({'success': False, 'error': 'Invalid role'})

    if role == 'mahasiswa' and student_status == '':
        print('mahasiswa but none status')
        return JsonResponse({'success': False, 'error': 'Student status can\'t be empty'})

    if role == 'staf' and jabatan == '':
        print('staf but none jabatan')
        return JsonResponse({'success': False, 'error': 'Staff role can\'t be empty'})

    if role == 'dosen' and jurusan == '':
        print('dosen but jurusan is none')
        return JsonResponse({'success': False, 'error': 'Professor faculty can\'t be empty'})

    if not is_university_id_exists(university_id):
        print('invalid university id')
        return JsonResponse({'success': False, 'error': 'Invalid university id'})

    return JsonResponse({'success': True})

@login_required(login_url="/login")
@csrf_exempt
def form_berita(request):
    response = {
        'page_title': 'Form Berita | BMNC'

    }

    print('WOIIIIIIIIIIIiii')

    if request.method == 'POST':
        judul = request.POST.get('judul_berita')
        topik_berita = request.POST.get('topik_berita')
        isi_berita = request.POST.get('isi_berita')
        tags_berita = request.POST.get('tag_berita')

        email = request.user.email
        # Create Berita
        create_new_berita(judul, topik_berita, tags_berita, isi_berita, email)

    return render(request, 'form_berita/index.html', response)

@login_required(login_url="/login")
@csrf_exempt
def polling_berita(request):
    response = {
        'page_title': 'Poll Berita | BMNC',
    }

    if request.method == 'POST':
        print(request.POST)
        url = request.POST.get('search_url_berita')
        time_start = request.POST.get('time_start')
        time_end = request.POST.get('time_end')

        list_polling = []
        if request.POST.get('polling-1') != None:
            list_polling.append(request.POST.get('polling-1'))

        if request.POST.get('polling-2') != None:
            list_polling.append(request.POST.get('polling-2'))

        if request.POST.get('polling-3') != None:
            list_polling.append(request.POST.get('polling-3'))

        if request.POST.get('polling-4') != None:
            list_polling.append(request.POST.get('polling-4'))

        print(url)
        print(time_start)
        print(time_end)
        print(list_polling)

        # Create Polling
        id_polling = create_new_polling(time_start, time_end)

        # Create Polling Berita
        response['flag'] = create_new_polling_berita(id_polling, url)

        # Create Respon
        create_new_respon(id_polling, list_polling)

    return render(request, 'polling_berita/index.html', response)

@login_required(login_url="/login")
@csrf_exempt
def polling_biasa(request):
    response = {
        'page_title': 'Poll Biasa | BMNC'
    }

    if request.method == 'POST':
        deskripsi = request.POST.get('deskripsi')
        url = request.POST.get('url_poll')
        time_start = request.POST.get('time_start')
        time_end = request.POST.get('time_end')

        list_polling = []
        if request.POST.get('polling-1') != None:
            list_polling.append(request.POST.get('polling-1'))

        if request.POST.get('polling-2') != None:
            list_polling.append(request.POST.get('polling-2'))

        if request.POST.get('polling-3') != None:
            list_polling.append(request.POST.get('polling-3'))

        if request.POST.get('polling-4') != None:
            list_polling.append(request.POST.get('polling-4'))

        print(request.POST)
        print(url)
        print(deskripsi)
        print(time_start)
        print(time_end)
        print(list_polling)

        # Create Polling
        id_polling = create_new_polling(time_start, time_end)

        # Create Polling Biasa
        response['flag'] = create_new_polling_biasa(id_polling, deskripsi, url)

        # Create Respon
        create_new_respon(id_polling, list_polling)

    return render(request, 'polling_biasa/index.html', response)

@login_required(login_url='/login/')
def profil(request):
    usercur = get_user_profil(request.user.email)
    response = {
        'page_title': 'Profil | BMNC',
        'username': request.user.username,
        'role': usercur[2],
        'profil': usercur[0],
        'roleattr': usercur[1]
    }

    return render(request, 'profil/index.html', response)

@login_required(login_url='/login/')
def daftar_berita(request):
    response = {
        'page_title': 'Berita Saya | BMNC',
        'beritas': get_berita_by_user(request.user.email)
    }

    return render(request, 'daftar_berita/index.html', response)

@login_required(login_url='/login/')
def daftar_polling(request):
    response = {
        'page_title': 'Daftar Polling | BMNC',
        'polls': get_polling_biasa()
    }

    if request.method == 'POST':
        jawaban = request.POST.get('answer')
        id_polling = request.POST.get('id_polling')
        ip_address = get_client_ip(request)

        submit_polls(id_polling, jawaban, ip_address)

    return render(request, 'daftar_polling/index.html', response)

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def tag_page(request, tag_name):
    response = {
        'page_title': '{} | BMNC'.format(tag_name.capitalize()),
        'beritas': get_berita_by_tag(tag_name),
        'tags': get_tag()
    }

    return render(request, 'homepage/index.html', response)
