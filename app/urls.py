from django.conf.urls import url
from .views import index, register, form_berita, polling_berita, polling_biasa, profil, daftar_berita, daftar_polling, tag_page, validate_register_request
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', index, name="homepage"),
    url(r'^login/$', auth_views.LoginView.as_view(redirect_authenticated_user=True,
                                                  template_name='login_page/index.html'), name="login-page"),
    url(r'^validate-registration/$', validate_register_request,
        name="validate-registration-data"),
    url(r'^register/$', register, name="register-page"),
    url(r'^logout/$', auth_views.logout,
        {'template_name': 'homepage/index.html', 'next_page': '/'}, name="logout"),
    url(r'^form_berita/$', form_berita, name="form_berita"),
    url(r'^polling_berita/$', polling_berita, name="polling_berita"),
    url(r'^polling_biasa/$', polling_biasa, name="polling_biasa"),
    url(r'^profile/', profil, name="profil"),
    url(r'^my-news/', daftar_berita, name="daftar_berita"),
    url(r'^polls/', daftar_polling, name="daftar_polling"),
    url(r'^tag/(?P<tag_name>.+)/$', tag_page, name="tag-page")
]
