from django.db import connection
from .models import Narasumber, Berita, Riwayat, Tag, Dosen, Staf, Mahasiswa, Polling, PollingBiasa, Narasumber, Riwayat, Tag, Universitas, Respon
from random import randint
from datetime import datetime


def create_new_narasumber(nama, email, tempat_lahir, tanggal_lahir, no_hp, id_universitas):
    latest_narasumber = Narasumber.objects.raw(
        'SELECT * FROM narasumber ORDER BY id DESC LIMIT 1')

    id = 1 if sum(
        1 for narasumber in latest_narasumber) == 0 else latest_narasumber[0].id + 1

    with connection.cursor() as cursor:
        cursor.execute(
            "INSERT INTO narasumber VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
            [id, nama, email, tempat_lahir, tanggal_lahir,
                no_hp, 0, 0, id_universitas]
        )
    return id


def create_new_mahasiswa(id_narasumber, npm, status):
    with connection.cursor() as cursor:
        cursor.execute(
            "INSERT INTO mahasiswa VALUES (%s, %s, %s)",
            [id_narasumber, npm, status]
        )


def create_new_staf(id_narasumber, nik, posisi):
    with connection.cursor() as cursor:
        cursor.execute(
            "INSERT INTO staf VALUES (%s, %s, %s)",
            [id_narasumber, nik, posisi]
        )


def create_new_dosen(id_narasumber, nik, jurusan):
    with connection.cursor() as cursor:
        cursor.execute(
            "INSERT INTO dosen VALUES (%s, %s, %s)",
            [id_narasumber, nik, jurusan]
        )


def get_berita():
    result = []
    query_berita = "SELECT * FROM berita WHERE url IN (SELECT url_berita FROM riwayat) ORDER BY created_at DESC"
    beritas = Berita.objects.raw(query_berita)

    for berita in beritas:
        latest_riwayat = Riwayat.objects.raw(
            "SELECT * FROM riwayat WHERE url_berita=%s ORDER BY waktu_revisi DESC LIMIT 1",
            [berita.url]
        )
        result.append((berita, latest_riwayat[0], randint(0, 50)))

    return result


def get_tag():
    query_tag = "SELECT tag FROM tag GROUP BY tag ORDER BY count(*) DESC LIMIT 10"
    tags = Tag.objects.raw(query_tag)

    return tags


def get_berita_by_tag(tag):
    result = []
    tagged_beritas = Tag.objects.raw("SELECT * FROM tag WHERE tag = %s", [tag])
    for tagged_berita in tagged_beritas:
        latest_riwayat = Riwayat.objects.raw(
            "SELECT * FROM riwayat WHERE url_berita=%s ORDER BY waktu_revisi DESC LIMIT 1",
            [tagged_berita.url_berita.url]
        )

        result.append((tagged_berita.url_berita,
                       latest_riwayat[0], randint(0, 50)))

    return  result


def get_berita_by_user(email):
    result = []
    query_berita = (
        '''SELECT * FROM berita, narasumber_berita
        WHERE id_narasumber=(SELECT id FROM narasumber WHERE email=%s)
        AND url=url_berita;''')
    beritas = Berita.objects.raw(query_berita, [email])

    for berita in beritas:
        latest_riwayat = Riwayat.objects.raw(
            "SELECT * FROM riwayat WHERE url_berita=%s ORDER BY waktu_revisi DESC LIMIT 1",
            [berita.url]
        )
        result.append(berita, latest_riwayat[0])

    return result


def get_polling_biasa():
    result = []
    pollings = Polling.objects.raw(
        "SELECT * FROM polling ORDER BY polling_end DESC")

    for polling in pollings:
        polling_biasas = PollingBiasa.objects.raw("SELECT * FROM polling_biasa WHERE id_polling=%s", [polling.id])
        if (sum(1 for polling_biasa in polling_biasas)==1):
            jawaban = Respon.objects.raw("SELECT * FROM respon WHERE id_polling=%s", [polling.id])
            result.append((polling_biasas, polling, jawaban))

    return result

def submit_polls(id_polling, jawaban, ip_address):
    respons = Respon.objects.raw("SELECT * FROM respon WHERE id_polling=%s AND jawaban=%s", [id_polling, jawaban])
    
    for respon in respons:
        jumlah_respon = respon.jumlah_dipilih
        jumlah_respon += 1

    with connection.cursor() as cursor:
        cursor.execute(
            '''UPDATE respon
            SET jumlah_dipilih=%s
            WHERE id_polling=%s AND jawaban=%s''',
            [jumlah_respon, id_polling, jawaban]
        )
        cursor.execute(
            "INSERT INTO responden VALUES (%s, %s)",
            [id_polling, ip_address]
        )

def get_user_profil(email):
    result = []
    query_profil = "SELECT * FROM narasumber WHERE email=%s"
    narasumber = Narasumber.objects.raw(query_profil, [email])
    result.append(narasumber)

    for narsum in narasumber:
        as_dosen = Dosen.objects.raw(
            "SELECT * FROM dosen WHERE id_narasumber=%s LIMIT 1", [narsum.id])
        as_staf = Staf.objects.raw(
            "SELECT * FROM staf WHERE id_narasumber=%s LIMIT 1", [narsum.id])
        as_mahasiswa = Mahasiswa.objects.raw(
            "SELECT * FROM mahasiswa WHERE id_narasumber=%s LIMIT 1", [narsum.id])

    if (sum(1 for dosen in as_dosen) == 1):
        result.append(as_dosen)
        result.append("Dosen")
    elif (sum(1 for staf in as_staf) == 1):
        result.append(as_staf)
        result.append("Staf")
    elif (sum(1 for mahasiswa in as_mahasiswa) == 1):
        result.append(as_mahasiswa)
        result.append("Mahasiswa")

    return result


def create_new_berita(judul, topik, tags, content, email):
    tags = tags.split(",")
    time_now = str(datetime.now())
    jumlah_kata = len(content.split(" "))

    latest_riwayat = Riwayat.objects.raw(
        "SELECT * FROM RIWAYAT ORDER BY ID_RIWAYAT DESC LIMIT 1")
    id_riwayat = 1 if sum(
        1 for i in latest_riwayat) else latest_riwayat[0].id + 1

    print(email)
    narasumber = Narasumber.objects.raw(
        "Select * FROM NARASUMBER WHERE email=%s", [email])
    print("hehe di raw")
    print(narasumber)
    id_univ = narasumber[0].id_universitas.id

    info_berita = Berita.objects.raw("SELECT * FROM berita")
    count_berita = sum(1 for berita in info_berita)
    url = "/news/" + str(count_berita)

    insert_berita = "INSERT INTO BERITA VALUES (%s, %s, %s, %s::timestamp, %s::timestamp, %s, %s, %s)"
    insert_riwayat = "INSERT INTO RIWAYAT VALUES (%s, %s, %s, %s)"
    insert_tag = "INSERT INTO TAG VALUES (%s, %s)"

    with connection.cursor() as cursor:
        cursor.execute(insert_berita, [
                       url, judul, topik, time_now, time_now, jumlah_kata, 0, id_univ])
        cursor.execute(insert_riwayat, [url, id_riwayat, time_now, content])

        for tag in tags:
            cursor.execute(insert_tag, [url, tag])


def create_new_polling(polling_start, polling_end):

    latest_polling = Polling.objects.raw(
        "SELECT * FROM POLLING ORDER BY ID DESC LIMIT 1")
    id_polling = 1 if sum(
        1 for i in latest_polling) == 0 else latest_polling[0].id + 1

    insert_polling = "INSERT INTO POLLING VALUES (%s,%s::timestamp,%s::timestamp,%s)"

    with connection.cursor() as cursor:
        cursor.execute(insert_polling, [
                       id_polling, polling_start, polling_end, 0])

    return id_polling


def create_new_polling_berita(id_polling, url_berita):
    search_berita = Berita.objects.raw(
        "SELECT * FROM BERITA WHERE url=%s", [url_berita])
    flag = False if sum(1 for i in search_berita) == 0 else True

    if flag:
        insert_polling_berita = "INSERT INTO POLLING_BERITA VALUES(%s,%s)"

        with connection.cursor() as cursor:
            cursor.execute(insert_polling_berita, [id_polling, url_berita])
        return not flag
    return not flag


def create_new_polling_biasa(id_polling, deskripsi, url):
    search_url = PollingBiasa.objects.raw(
        "SELECT * FROM POLLING_BIASA WHERE url=%s", [url])
    flag = True if sum(1 for i in search_url) == 0 else False

    insert_polling_biasa = "INSERT INTO POLLING_BIASA VALUES(%s,%s,%s)"

    if flag:
        with connection.cursor() as cursor:
            cursor.execute(insert_polling_biasa, [id_polling, url, deskripsi])
            return not flag
    return not flag


def create_new_respon(id_polling, jawaban):
    insert_respon = "INSERT INTO RESPON VALUES(%s,%s,%s)"

    with connection.cursor() as cursor:
        for jawab in jawaban:
            cursor.execute(insert_respon, [id_polling, jawab, 0])


def is_user_with_npm_or_nik_exists(id):
    dosen = Dosen.objects.raw(
        "SELECT * FROM DOSEN WHERE nik_dosen=%s LIMIT 1", [id])
    staf = Staf.objects.raw(
        "SELECT * FROM STAF WHERE nik_staf=%s LIMIT 1", [id])
    mahasiswa = Mahasiswa.objects.raw(
        "SELECT * FROM MAHASISWA WHERE npm=%s LIMIT 1", [id])

    return sum(1 for i in dosen) + sum(1 for i in staf) + sum(1 for i in mahasiswa) != 0


def is_university_id_exists(id):
    university = Universitas.objects.raw(
        "SELECT * FROM universitas WHERE id=%s", [id])

    return sum(1 for i in university) == 1
